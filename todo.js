/* global angular*/
angular.module('todoApp', [])
  .controller('TodoListController', function($scope, $http) {
    var todoList = this;
    var restBaseUrl = 'http://todo.devfellow.com:3006/api/todos/';

    todoList.refresh = function(){
        todoList.todos = [];
        $http.get(restBaseUrl).
            success(function(data) {
                todoList.todos = data;
            }).
            error(function(data){
            });
    };

    todoList.refresh();

    todoList.addTodo = function() {
        $http.post(restBaseUrl, {title:todoList.todoText, complete:null, completeDate:null}).
        success(function(data) {
            todoList.todos.push(data);
        }).
        error(function(data){

        });
      todoList.todoText = '';
    };

    todoList.updateTodo = function(todo){
        if(todo.complete){
            todo.completeDate = new Date();
        }else{
            todo.completeDate = null;
        }
        $http.put(restBaseUrl, todo).
        success(function(data) {
            //todoList.todos.push(data);
        }).
        error(function(data){

        });
      todoList.todoText = '';
    };

    todoList.remaining = function() {
      var count = 0;
      angular.forEach(todoList.todos, function(todo) {
        count += todo.complete ? 0 : 1;
      });
      return count;
    };

    todoList.archive = function() {
      angular.forEach(todoList.todos, function(todo) {
        if (todo.complete){ 
            $http.delete(restBaseUrl + todo.id).
            success(function(data){
                todoList.refresh();
            }).
            error(function(data){
                //todo
            });
        }
      });
    };
  });


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/